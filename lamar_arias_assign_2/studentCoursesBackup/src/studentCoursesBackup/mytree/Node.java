package studentCoursesBackup.mytree;
import java.util.ArrayList;
import studentCoursesBackup.mytree.ObserverI;
import studentCoursesBackup.mytree.SubjectI;
public class Node implements Cloneable{
	public ArrayList <String> course;
	public int bnum;
	public Node left, right;
	
	public SubjectI sub;
	
	public Node(int num, String list){
		course = new ArrayList();
		left = null;
		right = null;
		bnum = num;
		course.add(list);
	}
	//makes a copy of the node and the important parameters not including the sub and observers when used
	public Node clone(){
		Node tmp = new Node(bnum,course.get(0));
		tmp.course = (ArrayList<String>)course.clone();
		
		return tmp;
	}
	//makes subject object, not all nodes are subject
	public void makeSub(){
		sub = new SubjectI();
	}
	
}
