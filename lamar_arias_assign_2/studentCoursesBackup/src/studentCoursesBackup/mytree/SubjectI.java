package studentCoursesBackup.mytree;
import studentCoursesBackup.mytree.ObserverI;
import java.util.ArrayList;
public class SubjectI{
	//contains list of references to nodes
	public ArrayList <ObserverI> obsL;
	
	public SubjectI(){
		obsL = new ArrayList<ObserverI>();
	}
	
	public void addL(ObserverI n){
		obsL.add(n);
	}
	//goes through all node references and updates values
	public void notifyAll(Node u){
		for(int i = 0; i < obsL.size(); i++){
			obsL.get(i).update(u);
		}
	}

}
