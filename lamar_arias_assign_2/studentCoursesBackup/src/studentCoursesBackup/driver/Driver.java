package studentCoursesBackup.driver;
import studentCoursesBackup.util.FileProcessor;
import studentCoursesBackup.util.Treebuilder;
import studentCoursesBackup.util.Results;
import studentCoursesBackup.mytree.Node;
 public class Driver{
 
 
 public static void main(String [] args){
 	if(args.length == 5){// must be 5 args
 		FileProcessor inp = new FileProcessor();
 		inp.readLine(args[0]);
 		
 		//reads input file
 		Treebuilder orig = new Treebuilder();
 		orig.makebac();
 		//splits the string so the number and the course can be taken
 		for(int i = 0; i < inp.lines.size(); i++){
 			String delim = "[:]+";
        		String [] sp = inp.lines.get(i).split(delim);
        		int num = Integer.parseInt(sp[0]);
        		orig.insertS(num, sp[1]);
        		
 		}
 		//makes all results objects
 		Results o = new Results();
 		Results b1 = new Results();
 		Results b2 = new Results();
 		//reads delete file
 		FileProcessor del = new FileProcessor();
 		del.readLine(args[1]);
 		//parses file string so bnum and class can be used
 		for(int i = 0; i < del.lines.size(); i++){
 			String delim = "[:]+";
        		String [] sp = del.lines.get(i).split(delim);
        		int num = Integer.parseInt(sp[0]);
        		orig.deleteR(orig.root, num, sp[1]);
 		}
 		
 		//populates results objects with tree list in inorder traversal
 		if(orig.root != null){
 		o = orig.printNodes(o, orig.root);
 		b1 = orig.Back1.printNodes(b1, orig.Back1.root);
 		b2 = orig.Back2.printNodes(b2, orig.Back1.root);
 		}
 		
 		//write to outputs 1,2,3.txt or specified output files
 		o.writeToFile(args[2]);
 		b1.writeToFile(args[3]);
 		b2.writeToFile(args[4]);
 	}
 	else{
 		System.out.println("improper number of args, need 5");
 		System.exit(0);
 	}
 }
 
 }
