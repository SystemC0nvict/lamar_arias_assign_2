package studentCoursesBackup.util;
import java.io.*;
import java.util.*;

public class FileProcessor{
	public ArrayList <String> lines;
	public String [] classes = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"};
	public FileProcessor(){
		lines = new ArrayList<String>();
	}
	
	public void readLine(String inf) {
	try{
		File fi = new File(inf);
		FileReader re = new FileReader(fi);
		BufferedReader reader = new BufferedReader(new FileReader(inf));
		String line;
       	//try(reader = new BufferedReader(new FileReader(inf))){
        	//String line;
        	while((line = reader.readLine()) != null){
        		//if(line)
        		String delim = "[:]+";
        		String [] sp = line.split(delim);
        		if(Integer.parseInt(sp[0]) < 10000 && Arrays.asList(classes).contains(sp[1])){
        			//System.out.printf( " %s %s \n", sp[0],sp[1]);
        			lines.add(line);
        		}
        		else{
        			System.out.printf("improper input given: %s %s", sp[0], sp[1]);
        		}
        		
        	}
        re.close();
        //reader.close();
        }catch(IOException p){
        	p.printStackTrace();
        }
        //return line;
    }

}
