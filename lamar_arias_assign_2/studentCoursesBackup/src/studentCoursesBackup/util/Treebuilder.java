package studentCoursesBackup.util;
import java.util.ArrayList;
import studentCoursesBackup.mytree.Node;
import studentCoursesBackup.mytree.ObserverI;
import studentCoursesBackup.mytree.SubjectI;
import studentCoursesBackup.util.Results;
 public class Treebuilder{
 	public Node root;
 	public Treebuilder Back1;
 	public Treebuilder Back2;
 	
 	public Treebuilder(){
 		root = null;
 	}
 	//makes the backup trees
 	public void makebac(){
 		Back1 = new Treebuilder();
 		Back2 = new Treebuilder();
 	}
 	//insert method for main tree that takes in the bnum and class to be added
 	public void insertS(int num, String list){
 		root = insertSR(root, num, list);
 	}
 	//recursive insert method that inserts node into the main tree but also creates the clones of the node and inserts those clones into the backup trees. takes in the start node, bnum, and class to be added 
 	public Node insertSR(Node root, int num, String list){
 		if(root == null){
 			root = new Node(num, list);
 			
 			root.makeSub();
 			ObserverI back1 = new ObserverI();
 			back1.ob = root.clone();
 			ObserverI back2 = new ObserverI();
 			back2.ob = root.clone();
 			root.sub.addL(back1);
 			root.sub.addL(back2);
 			
 			Back1.insert(num, list, back1.ob);
 			Back2.insert(num, list,back2.ob);
 			return root;
 		}
 		
 		if(num < root.bnum)root.left = insertSR(root.left, num, list);
 		else if(num > root.bnum)root.right = insertSR(root.right, num, list);
 		else if(num == root.bnum){
 			root.course.add(list);
 			Back1.insert(num,list,root.sub.obsL.get(0).ob);
 			Back2.insert(num,list,root.sub.obsL.get(1).ob);
 		}
 		return root;
 	}
 	//insert function for backup trees. Takes in the bnum, class, and Node reference for the clone of the node to be inserted
 	public void insert(int num, String list, Node clo){
 		root = insertR(root, num, list,clo);
 	}
 	//recursive insert function for backup trees. Takes in start node, bnum, class, and node clone to be inserted 
 	public Node insertR(Node root, int num, String list, Node clo){
 		if(root == null){
 			return clo;
 		}
 		
 		if(num < root.bnum)root.left = insertR(root.left, num, list,clo);
 		else if(num > root.bnum)root.right = insertR(root.right, num, list,clo);
 		else if(num == root.bnum)root.course.add(list);
 		return root;
 	}
 	//function to search for node based on root and bnumber
 	public Node search(Node root,int num){
 		if(root == null || root.bnum == num)return root;
 		
 		if(root.bnum > num)return search(root.left, num);
 		
 		return search(root.right, num);
 	}
 	//delete function to delete which takes the bnum and class to delete. calls recursive delete function
 	public void delete(int n, String c){
 		root = deleteR(root, n,c);
 	}
 	//recursive delete function takes starting node, bnum, and class to delete
 	public Node deleteR(Node root, int num, String c){
 		if(root.bnum == num){
 			for(int i = 0; i < root.course.size(); i++){
 				if(root.course.get(i).equals(c)){
 					root.course.remove(i);
 					root.sub.notifyAll(root);
				}
 			}
 			return root;
 		}
 		if(root.left != null && root.bnum > num){
 			return deleteR(root.left, num, c);
 		}
 		if(root.right != null && root.bnum < num){
 			return deleteR(root.right, num, c);
 		}
 		return null;
 	}
 	//adds to the results string arraylist for final output and traverses in order
 	public Results printNodes(Results out, Node root){
 		if(root.left != null){
 			out = printNodes(out, root.left);
 		}
 		//used for formatting the string to add to the array
 		String p = Integer.toString(root.bnum); 
 		p = p +":";
 		for(int i = 0; i < root.course.size(); i++){
 			p = p + root.course.get(i);
 		}
 		out.output.add(p);

 		if(root.right != null){
 		out = printNodes(out, root.right);
 		}
 		return out;
 	}
 	
 	
 	
 	
 	
 
 }
