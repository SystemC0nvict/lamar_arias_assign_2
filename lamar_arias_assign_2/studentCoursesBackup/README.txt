Assuming you are in the directory containing this README:

## To clean:
ant -buildfile src/build.xml clean

-----------------------------------------------------------------------
## To compile: 
ant -buildfile src/build.xml all

-----------------------------------------------------------------------
## To run by specifying arguments from command line 
## We will use this to run your code
## the output files will be put the in src folder and the input files should also be there else the program will create them. 

ant -buildfile src/build.xml run -Darg0=src/input.txt -Darg1=src/delete.txt -Darg2=src/output1.txt -Darg3=src/output2.txt -Darg4=src/output3.txt

-----------------------------------------------------------------------

## To create tarball for submission
ant -buildfile src/build.xml tarzip

-----------------------------------------------------------------------

"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense.â€

[Date:10/3/17 ] -- Please add the date here

-----------------------------------------------------------------------
The Observer pattern was implemented by making the node class have a Subject object that contains an arraylist of Observers. When a node was created and before it was added to the main tree, two clones were created which were then used to insert into the backup trees. The clones passed in for the backup insert function are taken from an arraylist so when notify all is called and the clones are updated, the nodes in the backup trees are also updated as they were references of the clones.

The time complexity for BST insert, search, and delete was anywhere from O(n) to O(log n). The BST was easier to use because it is ordered which makes it easier to find values as well as do an inorder traversal for printing values.
Provide justification for Data Structures used in this assignment in
term of Big O complexity (time and/or space)


-----------------------------------------------------------------------
http://www.geeksforgeeks.org/binary-search-tree-set-1-search-and-insertion/
http://www.geeksforgeeks.org/binary-search-tree-set-2-delete/
https://stackoverflow.com/questions/513832/how-do-i-compare-strings-in-java
https://stackoverflow.com/questions/10117136/traversing-a-binary-tree-recursively
https://stackoverflow.com/questions/8441664/how-do-i-copy-the-contents-of-one-arraylist-into-another
http://pages.cs.wisc.edu/~hasti/cs302/examples/Parsing/parseString.html
http://www.avajava.com/tutorials/lessons/how-do-i-read-a-string-from-a-file-line-by-line.html
Provide list of citations (urls, etc.) from where you have taken code
(if any).












